import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {RequestsService} from '../../_services/requests.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Address} from '../../_models/address';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})
export class AddAddressComponent {

  public addressForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddAddressComponent>,
              @Inject(MAT_DIALOG_DATA) public id: number,
              private snackBar: MatSnackBar,
              private requestService: RequestsService) {
    this.addressForm = new FormGroup({
      adres_dostawy: new FormControl('', Validators.required),
      adres_platnosci: new FormControl('', Validators.required)
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit(): void {
    if (this.addressForm.hasError('required')) {
      return;
    }
    const address = new Address();
    address.id_klienta = this.id;
    address.adres_platnosci = this.addressForm.value.adres_platnosci;
    address.adres_dostawy = this.addressForm.value.adres_dostawy;
    this.requestService.addAddress(address)
      .subscribe(value => {
          this.snackBar.open('Adres dodany');
        },
        (error1: HttpErrorResponse) => {
          this.snackBar.open('Bład serwera numer ' + error1.status);
        });
    this.dialogRef.close();
  }

  public submitByEvent(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.submit();
    }
  }
}
