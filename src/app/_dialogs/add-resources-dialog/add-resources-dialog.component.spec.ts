import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddResourcesDialogComponent} from './add-resources-dialog.component';

describe('AddResourcesDialogComponent', () => {
  let component: AddResourcesDialogComponent;
  let fixture: ComponentFixture<AddResourcesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddResourcesDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddResourcesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
