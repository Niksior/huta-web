import {Component} from '@angular/core';
import {MatDialogRef, MatSnackBar} from '@angular/material';
import {RequestsService} from '../../_services/requests.service';
import {Resource} from '../../_models/resource';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-resources-dialog',
  templateUrl: './add-resources-dialog.component.html',
  styleUrls: ['./add-resources-dialog.component.scss']
})
export class AddResourcesDialogComponent {
  public resourceList: Resource[] | null;
  public resourceForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<AddResourcesDialogComponent>,
              private requestService: RequestsService,
              private snack: MatSnackBar) {
    this.resourceList = null;
    this.resourceForm = new FormGroup({
      miedz: new FormControl(''),
      nikiel: new FormControl(''),
      pallad: new FormControl(''),
      platyna: new FormControl(''),
      srebro: new FormControl(''),
      zloto: new FormControl('')
    });
    this.loadResources();
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  private loadResources(): void {
    this.requestService.getResources()
      .subscribe((value: Resource[]) => {
        this.resourceList = value;
      });
  }

  private updateResources() {
    if (this.resourceList) {
      this.requestService.updateResources(this.resourceList)
        .subscribe(value => {
            this.snack.open('Dodano');
          },
          (error1: HttpErrorResponse) => {
            this.snack.open('Błąd serwera nr: ' + error1.status);
          });
    }
  }

  private submit(): void {
    if (!this.resourceList) {
      return;
    }
    for (const resource of this.resourceList) {
      const control = this.resourceForm.controls[resource.nazwa_zasobu];
      if (control.value) {
        resource.ilosc += control.value;
      }
    }
    this.updateResources();
    this.dialogRef.close();
  }


}
