import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ManageClientDialogComponent} from './manage-client-dialog.component';

describe('ManageClientDialogComponent', () => {
  let component: ManageClientDialogComponent;
  let fixture: ComponentFixture<ManageClientDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageClientDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageClientDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
