import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {User} from '../../_models/user';
import {RequestsService} from '../../_services/requests.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Client} from '../../_models/client';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-manage-client-dialog',
  templateUrl: './manage-client-dialog.component.html',
  styleUrls: ['./manage-client-dialog.component.scss']
})
export class ManageClientDialogComponent {

  public clientForm: FormGroup;
  private client: Client;

  constructor(
    private dialogRef: MatDialogRef<ManageClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private user: User,
    private snackBar: MatSnackBar,
    private requestService: RequestsService
  ) {
    this.client = new Client();
    this.clientForm = new FormGroup({
      imie: new FormControl('', Validators.required),
      nazwisko: new FormControl('', Validators.required),
      nazwa_firmy: new FormControl('', Validators.required),
      numer_telefonu: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit(): void {
    if (this.clientForm.hasError('required') || this.clientForm.hasError('email')) {
      return;
    }
    this.client.imie = this.clientForm.value.imie;
    this.client.nazwisko = this.clientForm.value.nazwisko;
    this.client.nazwa_firmy = this.clientForm.value.nazwa_firmy;
    this.client.numer_telefonu = this.clientForm.value.numer_telefonu;
    this.client.email = this.clientForm.value.email;

    this.requestService.addClient(this.client)
      .subscribe((response: Client) => {
          this.snackBar.open('Klient dodany');
        },
        (error1: HttpErrorResponse) => {
          this.snackBar.open('Bład serwera numer ' + error1.status);
        });

    this.dialogRef.close();
  }

  public submitByEvent(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.submit();
    }
  }


}
