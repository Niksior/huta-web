import {Component, Inject} from '@angular/core';
import {User} from '../../_models/user';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RequestsService} from '../../_services/requests.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-manage-user-dialog',
  templateUrl: './manage-user-dialog.component.html',
  styleUrls: ['./manage-user-dialog.component.scss']
})
export class ManageUserDialogComponent {

  public userForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<ManageUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private user: User,
    private snackBar: MatSnackBar,
    private requestService: RequestsService
  ) {
    this.userForm = new FormGroup({
      login: new FormControl('', Validators.required),
      haslo: new FormControl('', Validators.required),
      admin: new FormControl('', Validators.required)
    });
    this.userForm.setValue({
      login: this.user.login,
      haslo: this.user.haslo,
      admin: this.user.administrator
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submit(): void {
    this.user.administrator = (this.userForm.value.admin === 'true');
    this.user.login = this.userForm.value.login;
    this.user.haslo = this.userForm.value.haslo;
    this.requestService.addUser(this.user)
      .subscribe(value => {
          this.snackBar.open('Użytkownik dodany');
        },
        (error1: HttpErrorResponse) => {
          if (error1.status === 409) {
            this.snackBar.open('Użytkownik już istnieje');
          } else {
            this.snackBar.open('Bład serwera numer ' + error1.status);
          }
        });
    this.dialogRef.close();
  }

}
