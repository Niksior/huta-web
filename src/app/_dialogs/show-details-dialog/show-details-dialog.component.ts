import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {RequestsService} from '../../_services/requests.service';
import {Details} from '../../_models/details';
import {Client} from '../../_models/client';
import {Order} from '../../_models/order';

@Component({
  selector: 'app-show-details-dialog',
  templateUrl: './show-details-dialog.component.html',
  styleUrls: ['./show-details-dialog.component.scss']
})
export class ShowDetailsDialogComponent {

  public detailList: Details[] | null;
  public client: Client | null;

  constructor(private dialogRef: MatDialogRef<ShowDetailsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public order: Order,
              private requestService: RequestsService) {
    this.detailList = null;
    this.client = null;
    this.loadDetails();
    this.loadClient();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private loadDetails(): void {
    this.requestService.getDetailsById(this.order.id_zamowienia)
      .subscribe((value: Details[]) => {
        this.detailList = value;
      });
  }

  private loadClient(): void {
    this.requestService.getClientById(this.order.id_klienta)
      .subscribe((value: Client) => {
        this.client = value;
      });
  }


}
