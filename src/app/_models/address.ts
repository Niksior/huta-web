export class Address {
  constructor() {
    this._id_klienta = 0;
    this._adres_dostawy = '';
    this._adres_platnosci = '';
  }

  private _id_klienta: number;

  get id_klienta(): number {
    return this._id_klienta;
  }

  set id_klienta(value: number) {
    this._id_klienta = value;
  }

  private _adres_dostawy: string;

  get adres_dostawy(): string {
    return this._adres_dostawy;
  }

  set adres_dostawy(value: string) {
    this._adres_dostawy = value;
  }

  private _adres_platnosci: string;

  get adres_platnosci(): string {
    return this._adres_platnosci;
  }

  set adres_platnosci(value: string) {
    this._adres_platnosci = value;
  }

}