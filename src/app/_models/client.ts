export class Client {
  constructor() {
    this._id_klienta = 0;
    this._imie = '';
    this._nazwisko = '';
    this._nazwa_firmy = '';
    this._numer_telefonu = '';
    this._email = '';
    this._data_rejestracji = '';
  }

  private _id_klienta: number;

  get id_klienta(): number {
    return this._id_klienta;
  }

  set id_klienta(value: number) {
    this._id_klienta = value;
  }

  private _imie: string;

  get imie(): string {
    return this._imie;
  }

  set imie(value: string) {
    this._imie = value;
  }

  private _nazwisko: string;

  get nazwisko(): string {
    return this._nazwisko;
  }

  set nazwisko(value: string) {
    this._nazwisko = value;
  }

  private _nazwa_firmy: string;

  get nazwa_firmy(): string {
    return this._nazwa_firmy;
  }

  set nazwa_firmy(value: string) {
    this._nazwa_firmy = value;
  }

  private _numer_telefonu: string;

  get numer_telefonu(): string {
    return this._numer_telefonu;
  }

  set numer_telefonu(value: string) {
    this._numer_telefonu = value;
  }

  private _email: string;

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  private _data_rejestracji: string;

  get data_rejestracji(): string {
    return this._data_rejestracji;
  }

  set data_rejestracji(value: string) {
    this._data_rejestracji = value;
  }
}