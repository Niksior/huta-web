export class Composition {
  constructor() {
    this._nazwa_produktu = '';
    this._zloto = 0;
    this._srebro = 0;
    this._pallad = 0;
    this._miedz = 0;
    this._nikiel = 0;
    this._platyna = 0;
  }

  private _nazwa_produktu: string;

  get nazwa_produktu(): string {
    return this._nazwa_produktu;
  }

  set nazwa_produktu(value: string) {
    this._nazwa_produktu = value;
  }

  private _zloto: number;

  get zloto(): number {
    return this._zloto;
  }

  set zloto(value: number) {
    this._zloto = value;
  }

  private _srebro: number;

  get srebro(): number {
    return this._srebro;
  }

  set srebro(value: number) {
    this._srebro = value;
  }

  private _pallad: number;

  get pallad(): number {
    return this._pallad;
  }

  set pallad(value: number) {
    this._pallad = value;
  }

  private _miedz: number;

  get miedz(): number {
    return this._miedz;
  }

  set miedz(value: number) {
    this._miedz = value;
  }

  private _nikiel: number;

  get nikiel(): number {
    return this._nikiel;
  }

  set nikiel(value: number) {
    this._nikiel = value;
  }

  private _platyna: number;

  get platyna(): number {
    return this._platyna;
  }

  set platyna(value: number) {
    this._platyna = value;
  }
}