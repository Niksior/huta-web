export class Details {

  constructor() {
    this._id_zamowienia = 0;
    this._nazwa_produktu = '';
    this._cena = 0;
    this._ilosc = 0;
    this._cena_kg = 0;
  }

  private _cena_kg: number;

  get cena_kg(): number {
    return this._cena_kg;
  }

  set cena_kg(value: number) {
    this._cena_kg = value;
  }

  private _id_zamowienia: number;

  get id_zamowienia(): number {
    return this._id_zamowienia;
  }

  set id_zamowienia(value: number) {
    this._id_zamowienia = value;
  }

  private _nazwa_produktu: string;

  get nazwa_produktu(): string {
    return this._nazwa_produktu;
  }

  set nazwa_produktu(value: string) {
    this._nazwa_produktu = value;
  }

  private _cena: number;

  get cena(): number {
    return this._cena;
  }

  set cena(value: number) {
    this._cena = value;
  }

  private _ilosc: number;

  get ilosc(): number {
    return this._ilosc;
  }

  set ilosc(value: number) {
    this._ilosc = value;
  }

}
