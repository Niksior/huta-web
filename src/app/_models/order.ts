export class Order {
  constructor() {
    this._id_zamowienia = 0;
    this._id_klienta = 0;
    this._cena = 0;
    this._data_dostawy = '';
    this._data_platnosci = '';
    this._adres_dostawy = '';
    this._adres_platnosci = '';
    this._czy_zaplacone = false;
    this._uwagi = '';
    this._data_zamowienia = '';
  }

  private _data_zamowienia: string;


  get data_zamowienia(): string {
    return this._data_zamowienia;
  }

  set data_zamowienia(value: string) {
    this._data_zamowienia = value;
  }

  private _id_zamowienia: number;

  get id_zamowienia(): number {
    return this._id_zamowienia;
  }

  set id_zamowienia(value: number) {
    this._id_zamowienia = value;
  }

  private _id_klienta: number;

  get id_klienta(): number {
    return this._id_klienta;
  }

  set id_klienta(value: number) {
    this._id_klienta = value;
  }

  private _cena: number;

  get cena(): number {
    return this._cena;
  }

  set cena(value: number) {
    this._cena = value;
  }

  private _data_dostawy: string;

  get data_dostawy(): string {
    return this._data_dostawy;
  }

  set data_dostawy(value: string) {
    this._data_dostawy = value;
  }

  private _data_platnosci: string;

  get data_platnosci(): string {
    return this._data_platnosci;
  }

  set data_platnosci(value: string) {
    this._data_platnosci = value;
  }

  private _adres_dostawy: string;

  get adres_dostawy(): string {
    return this._adres_dostawy;
  }

  set adres_dostawy(value: string) {
    this._adres_dostawy = value;
  }

  private _adres_platnosci: string;

  get adres_platnosci(): string {
    return this._adres_platnosci;
  }

  set adres_platnosci(value: string) {
    this._adres_platnosci = value;
  }

  private _czy_zaplacone: boolean;

  get czy_zaplacone(): boolean {
    return this._czy_zaplacone;
  }

  set czy_zaplacone(value: boolean) {
    this._czy_zaplacone = value;
  }

  private _uwagi: string;

  get uwagi(): string {
    return this._uwagi;
  }

  set uwagi(value: string) {
    this._uwagi = value;
  }
}
