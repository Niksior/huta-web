export class Product {
  constructor() {
    this._nazwa_produktu = '';
    this._cena = 0;
    this._czas_produkcji = 0;
  }

  private _nazwa_produktu: string;

  get nazwa_produktu(): string {
    return this._nazwa_produktu;
  }

  set nazwa_produktu(value: string) {
    this._nazwa_produktu = value;
  }

  private _cena: number;

  get cena(): number {
    return this._cena;
  }

  set cena(value: number) {
    this._cena = value;
  }

  private _czas_produkcji: number;

  get czas_produkcji(): number {
    return this._czas_produkcji;
  }

  set czas_produkcji(value: number) {
    this._czas_produkcji = value;
  }
}
