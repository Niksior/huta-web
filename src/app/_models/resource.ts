export class Resource {
  constructor() {
    this._nazwa_zasobu = '';
    this._ilosc = 0;
  }

  private _nazwa_zasobu: string;

  get nazwa_zasobu(): string {
    return this._nazwa_zasobu;
  }

  set nazwa_zasobu(value: string) {
    this._nazwa_zasobu = value;
  }

  private _ilosc: number;

  get ilosc(): number {
    return this._ilosc;
  }

  set ilosc(value: number) {
    this._ilosc = value;
  }
}
