export class User {
  private _login: string;
  private _haslo: string;
  private _bearer: string;
  private _administrator: boolean;

  constructor() {
    this._bearer = '';
    this._administrator = false;
    this._login = '';
    this._haslo = '';
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get haslo(): string {
    return this._haslo;
  }

  set haslo(value: string) {
    this._haslo = value;
  }

  get bearer(): string {
    return this._bearer;
  }

  set bearer(value: string) {
    this._bearer = value;
  }

  get administrator(): boolean {
    return this._administrator;
  }

  set administrator(value: boolean) {
    this._administrator = value;
  }
}
