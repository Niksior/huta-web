import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models/user';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  private currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    let storedUser = localStorage.getItem('currentUser');
    storedUser = (storedUser) ? storedUser : '[]';
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(storedUser));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public login(login: string, password: string): Observable<User> {
    return this.http.post<any>(`${environment.apiUrl}/api/v1/login`, {login: login, password: password})
      .pipe(map((user: User) => {
        if (user && user.bearer) {
          user.haslo = '';
          user.login = '';
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  public logout(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(new User());
  }
}
