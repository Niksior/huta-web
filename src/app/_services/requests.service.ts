import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from '../_models/user';
import {Observable} from 'rxjs';
import {Product} from '../_models/product';
import {Resource} from '../_models/resource';
import {Client} from '../_models/client';
import {Address} from '../_models/address';
import {Order} from '../_models/order';
import {Composition} from '../_models/composition';
import {Details} from '../_models/details';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient) {
  }

  /// user
  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/api/v1/users`);
  }
  public addUser(user: User): Observable<object> {
    return this.http.post<any>(`${environment.apiUrl}/api/v1/user`, user);
  }
  /// product
  public getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.apiUrl}/api/v1/products`);
  }
  /// resource
  public getResources(): Observable<Resource[]> {
    return this.http.get<Resource[]>(`${environment.apiUrl}/api/v1/resources`);
  }
  public updateResources(resources: Resource[]): Observable<Resource[]> {
    return this.http.put<Resource[]>(`${environment.apiUrl}/api/v1/resources`, resources);
  }
  /// client
  public getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.apiUrl}/api/v1/clients`);
  }

  public getClientById(id: number): Observable<Client> {
    return this.http.get<Client>(`${environment.apiUrl}/api/v1/client/` + id.toString());
  }
  public addClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.apiUrl}/api/v1/client`, client);
  }
  /// addresses
  public getAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>(`${environment.apiUrl}/api/v1/addresses`);
  }
  public getAddressesByUser(id: string): Observable<Address[]> {
    return this.http.get<Address[]>(`${environment.apiUrl}/api/v1/address/` + id);
  }
  public addAddress(address: Address): Observable<Address> {
    return this.http.post<Address>(`${environment.apiUrl}/api/v1/address`, address);
  }
  /// orders
  public getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(`${environment.apiUrl}/api/v1/orders`);
  }
  public addOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(`${environment.apiUrl}/api/v1/order`, order);
  }
  public updateOrder(order: Order): Observable<Order> {
    return this.http.put<Order>(`${environment.apiUrl}/api/v1/order`, order);
  }
  /// compositions
  public getCompositions(): Observable<Composition[]> {
    return this.http.get<Composition[]>(`${environment.apiUrl}/api/v1/compositions`);
  }
  /// details
  public addDetails(details: Details[]): Observable<Details[]> {
    return this.http.post<Details[]>(`${environment.apiUrl}/api/v1/details`, details);
  }

  public getDetailsById(id: number): Observable<Details[]> {
    return this.http.get<Details[]>(`${environment.apiUrl}/api/v1/details/` + id.toString());
  }
}
