import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../_services/requests.service';
import {Client} from '../_models/client';
import {Address} from '../_models/address';
import {MatDialog, MatSelectChange, MatSort, MatTableDataSource} from '@angular/material';
import {AddAddressComponent} from '../_dialogs/add-address/add-address.component';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss']
})
export class AddressesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<Address> | null;
  public columns: string[];
  public clientList: Client[] | null;
  public addressList: Address[] | null;
  public selected: number | null;

  constructor(private requestsService: RequestsService,
              private dialog: MatDialog) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = ['adres_dostawy', 'adres_platnosci'];
    this.clientList = null;
    this.addressList = null;
    this.selected = null;
    this.requestsService.getClients()
      .subscribe((clients: Client[]) => {
        this.clientList = clients;
      });
  }

  public showData(event: MatSelectChange) {
    this.loadTable(event.value);
  }

  public addAddress(): void {
    if (this.selected === null) {
      return;
    }
    const dialogRef = this.dialog.open(AddAddressComponent, {
      width: '400px',
      data: this.selected
    });
    dialogRef.afterClosed().subscribe(value => {
      if (this.selected !== null) {
        this.loadTable(this.selected);
      }
    });
  }

  private loadTable(id: number): void {
    this.requestsService.getAddressesByUser(id.toString())
      .subscribe((addresses: Address[]) => {
        if (addresses.length < 1) {
          const tmp = new Address();
          tmp.adres_dostawy = 'brak adresów';
          tmp.adres_platnosci = 'brak adresów';
          addresses.push(tmp);
        }
        this.dataSource = new MatTableDataSource(addresses);
        this.dataSource.sort = this.sort;
      });
  }

  ngOnInit() {
  }

}
