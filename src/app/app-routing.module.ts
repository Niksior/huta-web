import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './_guards/auth.guard';
import {UsersTableComponent} from './users-table/users-table.component';
import {ProductsComponent} from './products/products.component';
import {ResourcesComponent} from './resources/resources.component';
import {ClientTableComponent} from './client-table/client-table.component';
import {AddressesComponent} from './addresses/addresses.component';
import {OrderComponent} from './order/order.component';
import {AdminGuard} from './_guards/admin.guard';

const routes: Routes = [
  {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersTableComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: 'products', component: ProductsComponent, canActivate: [AuthGuard]},
  {path: 'resources', component: ResourcesComponent, canActivate: [AuthGuard]},
  {path: 'clients', component: ClientTableComponent, canActivate: [AuthGuard]},
  {path: 'addresses', component: AddressesComponent, canActivate: [AuthGuard]},
  {path: 'order', component: OrderComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
