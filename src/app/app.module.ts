import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavbarComponent} from './navbar/navbar.component';
import {LayoutModule} from '@angular/cdk/layout';
import {
  MAT_DATE_LOCALE,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './_interceptors/auth.interceptor';
import {UsersTableComponent} from './users-table/users-table.component';
import {ManageUserDialogComponent} from './_dialogs/manage-user-dialog/manage-user-dialog.component';
import {ProductsComponent} from './products/products.component';
import {ResourcesComponent} from './resources/resources.component';
import {ClientTableComponent} from './client-table/client-table.component';
import {ManageClientDialogComponent} from './_dialogs/manage-client-dialog/manage-client-dialog.component';
import {AddAddressComponent} from './_dialogs/add-address/add-address.component';
import {AddressesComponent} from './addresses/addresses.component';
import {DictionaryComponent} from './dictionary/dictionary.component';
import {OrderComponent} from './order/order.component';
import {registerLocaleData} from '@angular/common';
import localePl from '@angular/common/locales/pl';
import {InfoDialogComponent} from './_dialogs/info-dialog/info-dialog.component';
import {ShowDetailsDialogComponent} from './_dialogs/show-details-dialog/show-details-dialog.component';
import {AddResourcesDialogComponent} from './_dialogs/add-resources-dialog/add-resources-dialog.component';

registerLocaleData(localePl);

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    LoginComponent,
    UsersTableComponent,
    ManageUserDialogComponent,
    ProductsComponent,
    ResourcesComponent,
    ClientTableComponent,
    ManageClientDialogComponent,
    AddAddressComponent,
    AddressesComponent,
    DictionaryComponent,
    OrderComponent,
    InfoDialogComponent,
    ShowDetailsDialogComponent,
    AddResourcesDialogComponent
  ],
  entryComponents: [
    ManageUserDialogComponent,
    ManageClientDialogComponent,
    AddAddressComponent,
    DictionaryComponent,
    InfoDialogComponent,
    ShowDetailsDialogComponent,
    AddResourcesDialogComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatSlideToggleModule,
    MatSortModule,
    MatDialogModule,
    MatRadioModule,
    MatSelectModule,
    MatBottomSheetModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule
  ],
  providers: [
      {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: MAT_DATE_LOCALE, useValue: 'pl-PL'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
