import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../_services/requests.service';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Client} from '../_models/client';
import {ManageClientDialogComponent} from '../_dialogs/manage-client-dialog/manage-client-dialog.component';

@Component({
  selector: 'app-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.scss']
})
export class ClientTableComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<Client> | null;
  public columns: string[];

  constructor(private request: RequestsService,
              private dialog: MatDialog) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = ['id', 'imie', 'nazwisko', 'nazwa_firmy', 'numer_telefonu', 'email', 'data_rejestracji'];
    this.loadClients();
  }

  public addClient(): void {
    const dialogRef = this.dialog.open(ManageClientDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(value => {
      this.loadClients();
    });
  }

  public applyFilter(filterValue: string): void {
    if (this.dataSource) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

  ngOnInit() {
  }

  private loadClients(): void {
    this.request.getClients()
      .subscribe(clients => {
        this.dataSource = new MatTableDataSource(clients);
        this.dataSource.sort = this.sort;
      });
  }
}
