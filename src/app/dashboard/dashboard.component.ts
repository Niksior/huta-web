import {Component, ViewChild} from '@angular/core';
import {MatDialog, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {RequestsService} from '../_services/requests.service';
import {Order} from '../_models/order';
import {InfoDialogComponent} from '../_dialogs/info-dialog/info-dialog.component';
import {ShowDetailsDialogComponent} from '../_dialogs/show-details-dialog/show-details-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<Order> | null;
  public columns: string[];

  constructor(private request: RequestsService,
              private dialog: MatDialog,
              private snack: MatSnackBar) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = [
      'cena', 'data_dostawy', 'data_platnosci', 'adres_dostawy',
      'adres_platnosci', 'czy_zaplacone', 'uwagi',
      'data_zamowienia', 'szczegoly'
    ];
    this.getOrders();
  }

  public setPayment(order: Order) {
    order.czy_zaplacone = true;
    this.request.updateOrder(order)
      .subscribe(value => {

        },
        error1 => {
          this.snack.open('Ups, coś poszło nie tak');
        });
  }
  public showDetails(order: Order): void {
    this.dialog.open(ShowDetailsDialogComponent, {
      width: '400px',
      data: order
    });
  }
  public showInfo(info: string): void {
    this.dialog.open(InfoDialogComponent, {
      width: '400px',
      data: info
    });
  }
  private getOrders(): void {
    this.request.getOrders()
      .subscribe((orders: Order[]) => {
        this.dataSource = new MatTableDataSource(orders);
        this.dataSource.sort = this.sort;
      });
  }

  public applyFilter(filterValue: string): void {
    if (this.dataSource) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

}
