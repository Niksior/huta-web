import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../_services/auth.service';
import {MatSnackBar} from '@angular/material';
import {first} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading: boolean;
  public hide: boolean;
  public loginControl: FormControl;
  public passControl: FormControl;

  constructor(
    private router: Router,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
    this.loginControl = new FormControl('', Validators.required);
    this.passControl = new FormControl('', Validators.required);
    this.hide = true;
    this.loading = false;
  }

  public submit() {
    if (this.loginControl.hasError('required') || this.passControl.hasError('required')) {
      this.snackBar.open('Login i hasło są wymagane');
      return;
    }
    this.loading = true;
    this.authService.login(this.loginControl.value, this.passControl.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/']);
        },
        (error1: HttpErrorResponse) => {
          if (error1.status === 403) {
            this.snackBar.open('Błędny login lub hasło');
          } else {
            this.snackBar.open('Błąd serwera nr : ' + error1.status);
          }
          this.loading = false;
        }
      );
  }

  public submitByEvent(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.submit();
    }
  }

  ngOnInit() {
  }

}
