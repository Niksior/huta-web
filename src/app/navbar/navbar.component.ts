import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthService} from '../_services/auth.service';
import {Router} from '@angular/router';
import {User} from '../_models/user';
import {MatBottomSheet} from '@angular/material';
import {DictionaryComponent} from '../dictionary/dictionary.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {

  public toggleSidenav: boolean;
  public loggedUser: User;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private authService: AuthService,
              private router: Router,
              private bottomSheet: MatBottomSheet) {
    this.toggleSidenav = true;
    this.loggedUser = new User();
    this.router.events.subscribe(value => {
      this.toggleSidenav = !(this.router.url === ('/login'));
      if (this.toggleSidenav) {
        this.loggedUser = this.authService.currentUserValue;
      }
    });

  }

  public openDictionary(): void {
    this.bottomSheet.open(DictionaryComponent);
  }

  private logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
