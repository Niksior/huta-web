import {Component} from '@angular/core';
import {Client} from '../_models/client';
import {RequestsService} from '../_services/requests.service';
import {MatDialog, MatSelectionListChange, MatSnackBar} from '@angular/material';
import {ManageClientDialogComponent} from '../_dialogs/manage-client-dialog/manage-client-dialog.component';
import {Product} from '../_models/product';
import {Details} from '../_models/details';
import {Composition} from '../_models/composition';
import {Resource} from '../_models/resource';
import {Address} from '../_models/address';
import {AddAddressComponent} from '../_dialogs/add-address/add-address.component';
import {FormControl, Validators} from '@angular/forms';
import {Order} from '../_models/order';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {

  public selectedPaymentAddress: string | null;
  public selectedDeliveryAddress: string | null;
  public selectedClient: number | null;

  public addressList: Address[] | null;
  public resourceList: Resource[] | null;
  public productPickerList: Details[] | null;
  public clientList: Client[] | null;
  public productList: Product[] | null;
  private compositionList: Composition[] | null;
  private order: Order;
  public paymentDate: FormControl;
  public info: FormControl;
  public sum: number;
  public time: number;


  constructor(private requestService: RequestsService,
              private dialog: MatDialog,
              private snack: MatSnackBar) {
    this.order = new Order();
    this.info = new FormControl('');
    this.paymentDate = new FormControl('', [Validators.required]);
    this.selectedDeliveryAddress = null;
    this.selectedPaymentAddress = null;
    this.addressList = null;
    this.resourceList = null;
    this.compositionList = null;
    this.time = 0;
    this.sum = 0;
    this.productPickerList = null;
    this.productList = null;
    this.clientList = null;
    this.selectedClient = null;
    this.loadProducts();
    this.loadClients();
    this.loadCompositions();
    this.loadResources();
  }
  public sumAll(e: Event | null, nazwa: string): void {
    if (this.productPickerList) {
      this.sum = 0;
      for (const detail of this.productPickerList) {
        if (detail.nazwa_produktu === nazwa) {
          let difference = detail.ilosc;
          if (e && e.target) {
            // @ts-ignore
            detail.ilosc = parseFloat(e.target.value);
          } else {
            detail.ilosc = 0;
          }
          difference -= detail.ilosc;
          detail.cena = detail.cena_kg * detail.ilosc;
          this.updateResources(detail.nazwa_produktu, difference);
        }
        this.sum += detail.cena;
      }
    }
    this.updateTime();
  }
  public activatePicker(e: MatSelectionListChange, v: any) {
    if (this.productList) {
      for (const product of this.productList) {
        if (product.nazwa_produktu === e.option.value) {
          if (this.productPickerList) {
            let found = false;
            for (const detail of this.productPickerList) {
              if (detail.nazwa_produktu === product.nazwa_produktu) {
                const index = this.productPickerList.indexOf(detail, 0);
                if (index > -1) {
                  found = true;
                  this.sumAll(null, product.nazwa_produktu);
                  // this.time -= product.czas_produkcji;
                  this.productPickerList.splice(index, 1);
                }
              }
            }
            if (!found) {
              const detail = new Details();
              detail.cena_kg = product.cena;
              detail.nazwa_produktu = product.nazwa_produktu;
              // this.time += product.czas_produkcji;
              this.productPickerList.push(detail);
            }
          } else {
            this.productPickerList = [];
            const detail = new Details();
            detail.cena_kg = product.cena;
            detail.nazwa_produktu = product.nazwa_produktu;
            // this.time += product.czas_produkcji;
            this.productPickerList.push(detail);
          }
        }
      }
    }
  }
  /// final
  public submit(): void {
    if (this.checkIfResourcesExists()) {
      this.snack.open('Masz za mało materiałów w magazynie');
      return;
    }
    if (!this.productPickerList) {
      this.snack.open('Wybierz produkt/y');
      return;
    }
    for (const product of this.productPickerList) {
      if (product.ilosc <= 0) {
        this.snack.open('Wybierz ilość: ' + product.nazwa_produktu);
        return;
      }
    }
    if (!this.selectedClient) {
      this.snack.open('Wybierz klienta');
      return;
    }
    if (!this.selectedPaymentAddress || !this.selectedDeliveryAddress) {
      this.snack.open('Wybierz adres');
      return;
    }
    if (!this.paymentDate.value) {
      this.snack.open('Wybierz datę płatności');
      return;
    }
    this.order.cena = this.sum;
    this.order.adres_dostawy = this.selectedDeliveryAddress;
    this.order.adres_platnosci = this.selectedPaymentAddress;
    this.order.uwagi = this.info.value;
    this.order.id_klienta = this.selectedClient;
    this.order.data_platnosci = this.paymentDate.value;
    const today = new Date();
    today.setTime(today.getTime() + (this.time * 60 * 60 * 1000));
    this.order.data_dostawy = (today.toISOString());
    this.sendOrder();
  }
  private updateResources(name: string, amount: number): void {
    if (this.resourceList && this.compositionList) {
      for (const composition of this.compositionList) {
        if (composition.nazwa_produktu === name) {
          for (const resource of this.resourceList) {
            switch (resource.nazwa_zasobu) {
              case 'miedz':
                resource.ilosc += composition.miedz * amount;
                break;
              case 'nikiel':
                resource.ilosc += composition.nikiel * amount;
                break;
              case 'pallad':
                resource.ilosc += composition.pallad * amount;
                break;
              case 'platyna':
                resource.ilosc += composition.platyna * amount;
                break;
              case 'srebro':
                resource.ilosc += composition.srebro * amount;
                break;
              case 'zloto':
                resource.ilosc += composition.zloto * amount;
                break;
            }
          }
        }
      }
    }
  }
  /// buttons
  public addClient(): void {
    const dialogRef = this.dialog.open(ManageClientDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(value => {
      this.loadClients();
    });
  }
  public addAddress(): void {
    const dialogRef = this.dialog.open(AddAddressComponent, {
      width: '400px',
      data: this.selectedClient
    });
    dialogRef.afterClosed().subscribe(value => {
      this.loadAddresses();
    });
  }
  private updateTime(): void {
    this.time = 0;
    if (this.productPickerList && this.productList) {
      for (const detail of this.productPickerList) {
        for (const product of this.productList) {
          if (product.nazwa_produktu === detail.nazwa_produktu) {
            let tmp = product.czas_produkcji * detail.ilosc;
            if (tmp < 0) {
              tmp = Math.ceil(tmp);
            }
            this.time += Math.round(tmp);
          }
        }
      }
    }
  }
  private sendDetail(orderId: number): void {
    if (this.productPickerList) {
      for (const detail of this.productPickerList) {
        detail.id_zamowienia = orderId;
      }
      this.requestService.addDetails(this.productPickerList)
        .subscribe(value => {
          this.sendResources();
        });
    }
  }
  private checkIfResourcesExists(): boolean {
    let notExists = false;
    if (this.resourceList) {
      for (const resource of this.resourceList) {
        if (resource.ilosc < 0) {
          notExists = true;
          break;
        }
      }
    }
    return notExists;
  }
  private sendOrder(): void {
    this.requestService.addOrder(this.order)
      .subscribe((value: Order) => {
          this.snack.open('Zamówienie złożone');
          this.sendDetail(value.id_zamowienia);
        },
        (error1: HttpErrorResponse) => {
          this.snack.open('Błąd serwera nr: ' + error1.status);
        });
  }
  private sendResources(): void {
    if (this.resourceList) {
      this.requestService.updateResources(this.resourceList)
        .subscribe();
    }
  }

  /// loaders
  private loadClients(): void {
    this.requestService.getClients()
      .subscribe((value: Client[]) => {
        this.clientList = value;
      });
  }
  private loadProducts(): void {
    this.requestService.getProducts()
      .subscribe((value: Product[]) => {
        this.productList = value;
      });
  }
  private loadCompositions(): void {
    this.requestService.getCompositions()
      .subscribe((value: Composition[]) => {
        this.compositionList = value;
      });
  }
  private loadResources(): void {
    this.requestService.getResources()
      .subscribe((resources: Resource[]) => {
        this.resourceList = resources;
      });
  }
  private loadAddresses(): void {
    if (this.selectedClient) {
      this.requestService.getAddressesByUser(this.selectedClient.toString())
        .subscribe((value: Address[]) => {
          this.addressList = value;
        });
    }
  }
}
