import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Product} from '../_models/product';
import {RequestsService} from '../_services/requests.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<Product> | null;
  public columns: string[];

  constructor(private request: RequestsService) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = ['nazwa_produktu', 'cena', 'czas_produkcji'];
    this.request.getProducts()
      .subscribe(products => {
        this.dataSource = new MatTableDataSource(products);
        this.dataSource.sort = this.sort;
      });
  }

  public applyFilter(filterValue: string): void {
    if (this.dataSource) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

  ngOnInit() {
  }

}
