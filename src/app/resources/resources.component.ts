import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {RequestsService} from '../_services/requests.service';
import {Resource} from '../_models/resource';
import {AddResourcesDialogComponent} from '../_dialogs/add-resources-dialog/add-resources-dialog.component';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<Resource> | null;
  public columns: string[];

  constructor(private request: RequestsService,
              private dialog: MatDialog) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = ['nazwa_zasobu', 'ilosc'];
    this.loadResources();
  }

  public addResources(): void {
    const dialogRef = this.dialog.open(AddResourcesDialogComponent, {
      width: '400px'
    });
    dialogRef.afterClosed().subscribe(value => {
      this.loadResources();
    });
  }

  ngOnInit() {
  }

  private loadResources(): void {
    this.request.getResources()
      .subscribe(resources => {
        this.dataSource = new MatTableDataSource(resources);
        this.dataSource.sort = this.sort;
      });
  }
}
