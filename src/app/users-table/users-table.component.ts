import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../_services/requests.service';
import {User} from '../_models/user';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {ManageUserDialogComponent} from '../_dialogs/manage-user-dialog/manage-user-dialog.component';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<User> | null;
  public columns: string[];

  constructor(private request: RequestsService,
              private dialog: MatDialog) {
    this.sort = new MatSort();
    this.dataSource = null;
    this.columns = ['login', 'administrator'];
    this.loadUsers();
  }

  addUser(): void {
    const dialogRef = this.dialog.open(ManageUserDialogComponent, {
      width: '400px',
      data: new User()
    });
    dialogRef.afterClosed().subscribe(value => {
      this.loadUsers();
    });
  }

  ngOnInit() {

  }

  private loadUsers(): void {
    this.request.getUsers()
      .subscribe(users => {
        this.dataSource = new MatTableDataSource(users);
        this.dataSource.sort = this.sort;
      });
  }

}
